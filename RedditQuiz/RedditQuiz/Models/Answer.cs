﻿namespace RedditQuiz.Views
{
    public class Answer
    {
        private bool correct;
        private string text;

        public Answer()
        {
            this.correct = false;
            this.text = "This is a very long Placeholder Answer Text. This is a very long Placeholder Answer Text. This is a very long Placeholder Answer Text. " +
                "This is a very long Placeholder Answer Text. This is a very long Placeholder Answer Text. This is a very long Placeholder Answer Text. This is a very" +
                " long Placeholder Answer Text. This is a very long Placeholder Answer Text.";
        }

        public Answer(string text, bool correct)
        {
            this.correct = correct;
            this.text = text;
        }

        public string getText()
        {
            return this.text;
        }

        public bool isCorrect()
        {
            return this.correct;
        }

        public void setText(string i)
        {
            this.text = i;
        }

        public void setCorrect(bool i)
        {
            this.correct = i;
        }

    }

}