﻿using System;
using System.Linq;

namespace RedditQuiz.Views
{
    public class Question {
        private string question;
        private Answer[] answers;


        public Question()
        {
            answers = new Answer[4];

            for(int i = 0; i < 4; i++)
            {
                answers[i] = new Answer();
            }

            this.question = "I am a question";

        }


        public Question(string ques)
        {
            answers = new Answer[4];

            for (int i = 0; i < 4; i++)
            {
                answers[i] = new Answer();
            }

            this.question = ques;

        }

        public string getQuestion()
        {
            return this.question;
        }

        public Answer getAnswer(int i)
        {
            return answers[i];
        }

        public void setAnswer(int i, Answer answerIn)
        {
            this.answers[i] = answerIn;
        }

        public void randAnswers()
        {
            Random rnd = new Random();
            this.answers = this.answers.OrderBy(x => rnd.Next()).ToArray();
        }

    }

}