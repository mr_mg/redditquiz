﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedditQuiz.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuestionPage : ContentPage
    {
        
        public Question currentQuestion;


        public QuestionPage()
        {

            InitializeComponent();

            currentQuestion = new Question();

            questionText.Text = currentQuestion.getQuestion();


            currentQuestion.randAnswers();

            answerButton1.Text = currentQuestion.getAnswer(0).getText();
            answerButton2.Text = currentQuestion.getAnswer(1).getText();
            answerButton3.Text = currentQuestion.getAnswer(2).getText();
            answerButton4.Text = currentQuestion.getAnswer(3).getText();

        }

        private void onAnswerBox1Clicked(object sender, EventArgs e)
        {

        }

        private void onAnswerBox2Clicked(object sender, EventArgs e)
        {

        }

        private void onAnswerBox3Clicked(object sender, EventArgs e)
        {

        }

        private void onAnswerBox4Clicked(object sender, EventArgs e)
        {

        }
    }

}