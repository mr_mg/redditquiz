﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedditQuiz.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AnswerResultPage : ContentPage
	{
		public AnswerResultPage ()
		{
			InitializeComponent ();
		}
	}
}