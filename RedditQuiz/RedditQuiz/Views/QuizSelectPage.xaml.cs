﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedditQuiz.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QuizSelectPage : ContentPage
	{
		public QuizSelectPage ()
		{
			InitializeComponent ();
		}

        private async Task OnButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}